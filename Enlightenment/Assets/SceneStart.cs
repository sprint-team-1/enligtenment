﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneStart : MonoBehaviour
{
    public FadeToBlack fadeToBlack;


    // Start is called before the first frame update
    void Start()
    {
        fadeToBlack.StartBlackOut();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
