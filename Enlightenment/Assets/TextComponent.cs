﻿using Febucci.UI;
using UnityEngine;

public class TextComponent : MonoBehaviour
{
    public TextAnimatorPlayer textAnimatorPlayer;
    public FadeToBlack fadeToBlack;

    // Start is called before the first frame update
    private void Start()
    {
        textAnimatorPlayer.textAnimator.onEvent += OnEvent;
    }

    private void OnEvent(string message)
    {
        switch (message)
        {
            case "end":
                fadeToBlack.StopBlackOut();
                textAnimatorPlayer.StopShowingText();
                textAnimatorPlayer.ShowText("");
                break;
        }
    }
}