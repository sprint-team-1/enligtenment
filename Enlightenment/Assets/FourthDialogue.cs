﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Febucci.UI;

public class FourthDialogue : MonoBehaviour
{
    public TextAnimatorPlayer textAnimatorPlayer;
    public FadeToBlack fadeToBlack;
    bool hasEntered;

    [FMODUnity.EventRef]
    public string inputSound;




    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player" && !hasEntered)
        {
            textAnimatorPlayer.ShowText(textToShow);
            fadeToBlack.StartBlackOut();
            ShowText();
            hasEntered = true;

        }
    }

    [TextArea(3, 50), SerializeField]
    string textToShow = " ";

    private void Awake()
    {
        UnityEngine.Assertions.Assert.IsNotNull(textAnimatorPlayer, $"Text Animator Player component is null in {gameObject.name}");

    }

    public void ShowText()
    {
        textAnimatorPlayer.ShowText(textToShow);
    }
}