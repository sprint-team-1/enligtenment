﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeToBlack : MonoBehaviour
{   
    public Image blackOutSquare;
    Color objectColor;

    public void Start()
    {
        blackOutSquare = GetComponent<Image>();
        Debug.Log(blackOutSquare);
        objectColor = blackOutSquare.color;
    }

    public void StartBlackOut()
    {
        StartCoroutine(FadeBlackOutSquare());
    }

    public void StopBlackOut()
    {
        StartCoroutine(FadeBlackOutSquare(false));
    }


    public IEnumerator FadeBlackOutSquare(bool fadeToBlack = true, int fadeSpeed = 5)
    {
        
        float fadeAmount;

        if (fadeToBlack)
        {
            while(blackOutSquare.color.a < 1)
            {
                fadeAmount = objectColor.a + (fadeSpeed * Time.deltaTime);

                objectColor = new Color(objectColor.r, objectColor.g, objectColor.b, fadeAmount);
                blackOutSquare.color = objectColor;
                yield return null;
            }
        }
        else
        {
            while (blackOutSquare.color.a > 0)
            {
                fadeAmount = objectColor.a - (fadeSpeed * Time.deltaTime);

                objectColor = new Color(objectColor.r, objectColor.g, objectColor.b, fadeAmount);
                blackOutSquare.color = objectColor;
                yield return null;
            }
        }
    }
}
