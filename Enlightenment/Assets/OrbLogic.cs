﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbLogic : MonoBehaviour
{

    public GameObject orbLocation;
    public GameObject player;
    float timer = 2;
    bool startTimer;
    bool startMovement;

    // Start is called before the first frame update
    void Awake()
    {
        startTimer = true;
        startMovement = true;
        orbLocation = GameObject.Find("OrbLocation");
        player = GameObject.Find("Player");
        
    }

    private void Update()
    {

        if (startTimer == true)
        {
            timer -= Time.deltaTime;
        }

        if(timer > 0)
        {
            Move();
        }

        if (timer < 0)
        {
            MoveOrb();
        }
    }

    private void Move()
    {
        float x = Mathf.Lerp(transform.position.x, Random.insideUnitCircle.x + player.transform.position.x, 0.1f);
        float y = Mathf.Lerp(transform.position.y, Random.insideUnitCircle.y + player.transform.position.y, 0.1f);
        transform.position = new Vector2(x, y);
    }

    private void MoveOrb()
    {

        Debug.Log("Moving");
        float x = Mathf.Lerp(transform.position.x, orbLocation.transform.position.x, 1 * Time.deltaTime);
        float y = Mathf.Lerp(transform.position.y, orbLocation.transform.position.y, 1 * Time.deltaTime);
        transform.position = new Vector2(x, y);

        float distance = Vector3.Distance(transform.position, orbLocation.transform.position);

        if(distance < 3)
        {
            float alpha =- 0.1f;
            GetComponent<SpriteRenderer>().color = new Color (1,1,1, alpha);
        }
       


    }
}
