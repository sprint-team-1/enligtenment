﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BossEnergy : MonoBehaviour
{
    public Slider slider;

    public Image fillValue;

    public PlayerController playerC;

    private void Start()
    {
        playerC.GetComponent<PlayerController>();

        slider.maxValue = 100;
        slider.value = 0;
    }

    public void SetMaxEnergy(float energy)
    {
        // set the max health value at the start of the game, according to the max value
        slider.maxValue = energy;
        slider.value = energy;
    }

    public void Update()
    {
        if (slider.value == slider.maxValue)
        {
            SceneManager.LoadScene(1);
        }
    }
}