﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBehaviour : MonoBehaviour
{
    
    public Collider2D collider2D;
    public GameObject projectilePrefab;
    public GameObject swipeAttackPrefab;
    public Animator animatorBoss;
    public Animator animatorEye;
    public Animator animatorRings;
    public GameObject player;
    public GameObject lightEffect;
    public GameObject shrineRight;
    public GameObject shrineLeft;
    private GameObject shrineClone;
    private PlayerController pC;

    [Header("Omni Attack Variables")]
    public int omniDensity;
    public float omniProjectileSpeed;
    public int maxOmniWaves;

    [Header("Spiral Attack Variables")]
    public int spiralDensity;
    public float spiralProjectileSpeed;
    public int maxSpiralProjectiles;
    private float currentSpiralDegreeOffset;

    [HideInInspector]public bool spawnArm = false;

    [Header("General Variables")]
    public float projectileLifetime;
    public int attackWaveCount;
    public float restLengthInSec;
    
    public static bool shrineActiveRight = false;
    public static bool shrineActiveLeft = false;
    public Sprite shrineActiveSprite;
    private Sprite savedCurrentSprite;

    private void Start()
    {
        savedCurrentSprite = GetComponentInChildren<SpriteRenderer>().sprite;

        pC = player.GetComponent<PlayerController>(); 
    }

    private void Update()
    {
        if (shrineActiveRight)
        {
            GetComponentInChildren<SpriteRenderer>().sprite = shrineActiveSprite;
        }
        else 
        {
            GetComponentInChildren<SpriteRenderer>().sprite = savedCurrentSprite;
        }
        if (shrineActiveLeft)
        {
            GetComponentInChildren<SpriteRenderer>().sprite = shrineActiveSprite;
            GetComponentInChildren<SpriteRenderer>().flipX = true;
        }
        else
        {
            GetComponentInChildren<SpriteRenderer>().sprite = savedCurrentSprite;
        }

    }
    public void OmnidirectionalAttack()
    {
        for (int currentProjectileCount = 0; currentProjectileCount <= omniDensity; currentProjectileCount++)
        {
            float radius = collider2D.bounds.extents.y;
            float degree = Mathf.Atan(((2 * Mathf.PI * radius) / omniDensity) / radius);

            Vector2 spawnProjectilePos = new Vector2((Mathf.Sin(degree * currentProjectileCount) * radius) + transform.position.x, (Mathf.Cos(degree * currentProjectileCount) * radius) + transform.position.y);
            GameObject bullet = Instantiate(projectilePrefab, spawnProjectilePos, Quaternion.identity);
            bullet.transform.parent = gameObject.transform;


            Vector2 shootDir = (bullet.transform.position - transform.position);
            bullet.GetComponent<Rigidbody2D>().velocity = new Vector2(omniProjectileSpeed * shootDir.x, omniProjectileSpeed * shootDir.y);

            Destroy(bullet, projectileLifetime);
        }
    }

    public void SpiralAttack()
    {    
        float radius = collider2D.bounds.extents.y;
        float degree = Mathf.Atan(((2 * Mathf.PI * radius) / spiralDensity) / radius);

        degree = degree + currentSpiralDegreeOffset;

        Vector2 spawnProjectilePos = new Vector2((Mathf.Sin(currentSpiralDegreeOffset * spiralDensity) * radius) + transform.position.x, (Mathf.Cos(currentSpiralDegreeOffset * spiralDensity) * radius) + transform.position.y);
        GameObject bullet = Instantiate(projectilePrefab, spawnProjectilePos, Quaternion.identity);
        bullet.transform.parent = gameObject.transform;


        Vector2 shootDir = (bullet.transform.position - transform.position);
        bullet.GetComponent<Rigidbody2D>().velocity = new Vector2(spiralProjectileSpeed * shootDir.x, spiralProjectileSpeed * shootDir.y);

        Destroy(bullet, projectileLifetime);
    }

    public void SweepAttack()
    {
        if (GameObject.Find("SweepBar(Clone)") == null)
        {
            GameObject swipeBar = Instantiate(swipeAttackPrefab, transform.position, Quaternion.AngleAxis(2, Vector3.forward));
            swipeBar.transform.parent = gameObject.transform;

            spawnArm = true;
        }
        else if (GameObject.Find("SweepBar(Clone)") != null && GameObject.Find("SweepBar(Clone)").transform.rotation == transform.rotation)
        {
            spawnArm = false;
        }
    }

    public void ShrineRight()
    {
        if (GameObject.Find("Pillar(Clone)") == null && shrineActiveRight == false)
        {
            shrineClone = Instantiate(lightEffect, shrineRight.transform.position, Quaternion.identity);
            shrineActiveRight = true;
            return;
        }
        else if (GameObject.Find("Pillar(Clone)") != null && shrineActiveRight == true)
        {
            Destroy(shrineClone);
            shrineActiveRight = false;
            pC.shrineTimer = 0;
            return;
        }
        else
            return;
    }

    public void ShrineLeft()
    {
        if (GameObject.Find("Pillar(Clone)") == null && shrineActiveLeft == false)
        {
            shrineClone = Instantiate(lightEffect, shrineLeft.transform.position, Quaternion.identity);
            shrineActiveLeft = true;
            return;
        }
        else if (GameObject.Find("Pillar(Clone)") != null && shrineActiveLeft == true)
        {
            Destroy(shrineClone);
            shrineActiveLeft = false;
            pC.shrineTimer = 0;
            return;
        }
        else
            return;
    }
}
