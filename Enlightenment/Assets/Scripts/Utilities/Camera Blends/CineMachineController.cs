﻿using UnityEngine;


public class CineMachineController : MonoBehaviour
{

    private Animator animator;
    private bool followCharacter = true;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        animator.Play("PlayerCamera");
    }
    // Start is called before the first frame update

     public void PanToBoss()
    {
            animator.Play("BossCamera");
    }

    public void PanToPlayer()
    {
        animator.Play("PlayerCamera");
    }

}
