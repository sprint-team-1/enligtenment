﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class Cutscene1 : MonoBehaviour
{
    private CineMachineController cmController;
    private BoxCollider2D boxCollider2D;
    [SerializeField] private float cutsceneDuration;

    bool startTimer = false;
    float timer;
    bool canPlay = true;

    [FMODUnity.EventRef]
    public string BossMusic;

    private void Start()
    {
        cmController = GetComponentInParent<CineMachineController>();
        boxCollider2D = GetComponent<BoxCollider2D>();
        boxCollider2D.isTrigger = true;
        timer = cutsceneDuration;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player" && canPlay)
        {
            Debug.Log("Triggered");
            cmController.PanToBoss();
            startTimer = true;
            FMODUnity.RuntimeManager.PlayOneShot(BossMusic);
            canPlay = false;
        }
    }

    private void Update()
    {
        if (startTimer)
        {
            timer -= Time.deltaTime;

            if(timer <= 0)
            {
                EndCutScene();
            }
        }
    }

    private void EndCutScene()
    {
        cmController.PanToPlayer();
    }
}
