﻿using UnityEngine;

public static class PlayerInput
{
    public const string AXIS_HORIZONTAL = "Horizontal";
    public const string AXIS_VERTICAL = "Vertical";
    public const string TRIGGER_BUTTON = "Fire1";

    /// <summary>
    /// Provides the current input from directional controls (arrow keys, d-pad, left joystick).
    /// </summary>
    /// <returns>A raw Vector2 with horizontal input stored in the x axis and vertical input stored int eh y axis.</returns>
    public static Vector2 GetDirectionalInput()
    {
        return new Vector2(Input.GetAxisRaw(AXIS_HORIZONTAL), Input.GetAxisRaw(AXIS_VERTICAL));
    }

    public static bool WasAttackPressed()
    {
        return Input.GetButtonDown(TRIGGER_BUTTON);
    }
}