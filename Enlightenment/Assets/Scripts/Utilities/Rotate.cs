﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    public float length;
    //float speed = (2 * Mathf.PI) / 5; //2*PI in degress is 360, so you get 5 seconds to complete a circle

    void Update()
    {
        float speed = (2 * Mathf.PI) / length;

        transform.Rotate(new Vector3(0, 0, speed), Space.World);

        if (Quaternion.Angle(this.transform.rotation, transform.parent.gameObject.transform.rotation) < 1)
        {            
            Destroy(gameObject);
        }
    }
}
