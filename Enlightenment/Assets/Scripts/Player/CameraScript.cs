﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    [SerializeField] private Camera cam;
    private float targetZoom;
    private float zoomFactor = 3f;
    private float zoomLerpSpeed = 10;

    public float zoomData;

    public List<Transform> targets;
    public GameObject player;
    public Vector3 mousePos;

    public float smoothTime;

    public float minZoom = 2.5f;
    public float maxZoom = 10.0f;

    private Vector3 velocity;

    
    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
        targetZoom = cam.orthographicSize;

        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        //targetZoom -= zoomData * zoomFactor;
        //targetZoom = Mathf.Clamp(targetZoom, maxZoom, minZoom);
        //cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, targetZoom, Time.deltaTime * zoomLerpSpeed);
        if (getDistanceBetweenTargets() > 17.0f)
        {
            if (mousePos.x < player.transform.position.x)
            {
                mousePos.x = player.transform.position.x - 17.0f;
            }
            else mousePos.x = player.transform.position.x + 17.0f;

            if (mousePos.y < player.transform.position.y)
            {
                mousePos.y = player.transform.position.y - 17.0f;
            }
            else mousePos.y = player.transform.position.y + 17.0f;
        }
        Move();
        Zoom();
        
    }

    private void Update()
    {
        mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        //Debug.Log(mousePos);
    }

    Vector3 getCenterPoint()
    {
        //this function gets center point between player and mouse position
        Vector3 center = new Vector3((player.transform.position.x + mousePos.x)/2.0f, (player.transform.position.y + mousePos.y)/2.0f);
        return center;

    }

    void Zoom()
    {
        float newZoom = Mathf.Lerp(minZoom, maxZoom, getDistanceBetweenTargets() / 20.0f); //for value to be between 1 & 0, divide distance
        cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, newZoom, Time.deltaTime * 10.0f);
    }

    void Move()
    {

        Vector3 centerPoint = getCenterPoint();
        transform.position = Vector3.SmoothDamp(transform.position, centerPoint + new Vector3(0, 0, -0.4f), ref velocity, 0.5f); //adding offset to camera
    }

    float getDistanceBetweenTargets()
    {
        return Vector3.Distance(player.transform.position, mousePos);
    }
}
