﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public enum FacingDirection { Left, Right }
    public enum RollDirection { Up, LeftRight, Down, DoNothing }

    private Vector2 directionInput;
    public GameObject orbs;
    public GameObject orbLocation;
    public GameObject ring;
    [SerializeField] FlashImage _flashImage = null;
    [SerializeField] Color _newcolor = Color.white;

    [Header("FMod Audio")]
    [FMODUnity.EventRef]
    public string Updrade1;
    [FMODUnity.EventRef]
    public string Updrade2;
    [FMODUnity.EventRef]
    public string Updrade3;


    [Header("Movement Variables")]
    [SerializeField] private float m_accelerationTimeFromRest;
    [SerializeField] private float m_decelerationTimeToRest;
    [SerializeField] private float m_maxHorizontalSpeed;

    //[SerializeField] Transform spawnPos;

    private float currentSpeed;
    private float movementVelocityX;
    private float movementVelocityY;
    private float acceleration;
    private float deceleration;
    private float savedDirection;


    ///Variables for all the dashing in the game in the game
    [Header("Dashing Variables")]
    [SerializeField] private float dashTime;
    [SerializeField] private float dashSpeed;
    [SerializeField] private float dashDeaccelerationTime;

    private float dashDeacellerationX;
    private float dashDeacellerationY;
    private float dashSpeedX;
    private float dashSpeedY;

    private float dashTimeLeft;
    private bool endOfDash;
    private float endOfDashtimer;
    public static bool canDash = true;
    private bool isDashing;
    private bool canMove;
    private Vector3 mousePos;
    private Camera myCamera;
    private Vector3 mouseDirection;
    private bool overrideDash = false;
    private Vector3 shrine;
    private bool triggerAttack;
    private bool shrineTrigger;
    bool disableInput;
    bool hasClicked = true;
    bool moveDown;
    GameObject orbClones;
    int clicks;


    [SerializeField] public float shrineTimer;
    private bool startShrineTimer;

    public BossBehaviour bossB;
    public BossEnergy bossE;
    public float bossEnergyClicks = 0f;
    public Slider slider;

    private void Start()
    {
        bossB = GetComponent<BossBehaviour>();
        bossE = GetComponent<BossEnergy>();
    }
    public bool IsWalkingLeftRight()
    {
        Vector2 input = PlayerInput.GetDirectionalInput();
        if (Mathf.Abs(input.x) > 0 && !disableInput)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool IsWalkingUp()
    {
        Vector2 input = PlayerInput.GetDirectionalInput();
        if (input.y > 0 && !disableInput)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool IsWalkingDown()
    {
        Vector2 input = PlayerInput.GetDirectionalInput();
        if (input.y < 0 && !disableInput)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public FacingDirection GetFacingDirection()
    {
        if (movementVelocityX > 0 )
        {
            return FacingDirection.Right;
        }
        if(movementVelocityX == 0 && savedDirection == 1)
        {
            return FacingDirection.Right;
        }
        if(movementVelocityX == 0 && savedDirection == -1)
        {
            return FacingDirection.Left;
        }
        else 
        {
            return FacingDirection.Left;
        }
    }

    public bool RollLeftRight()
    {
        if((mouseDirection.x > 0 || mouseDirection.x < 0) && !canDash)
        {
            return true;
        }else
        {
            return false;
        }
    }
    public bool RollDown()
    {
        if ((mouseDirection.x > 0 || mouseDirection.x < 0) && !canDash)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool RollUp()
    {
        if (mouseDirection.y > 0 && !canDash)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void Update()
    {
      
        //playerCam.transform.position = new Vector3(this.gameObject.transform.position.x, this.transform.position.y, -0.4f);
        GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;

        if ((Mathf.Abs(PlayerInput.GetDirectionalInput().x) > 0))
        {
            directionInput.x = PlayerInput.GetDirectionalInput().x;
            savedDirection = directionInput.x;
        }
        else
        {
            directionInput.x = 0;
        }

        if ((Mathf.Abs(PlayerInput.GetDirectionalInput().y) > 0))
        {
            directionInput.y = PlayerInput.GetDirectionalInput().y;
        }
        else
        {
            directionInput.y = 0;
        }

        //Calls the method to start the dash within the parameter of the dash variables
        if (Input.GetButtonDown("Dash") && canDash && !overrideDash)
        {
            myCamera = Camera.main;
            mousePos = myCamera.ScreenToWorldPoint(Input.mousePosition);
            mouseDirection = (mousePos - this.transform.position);
            mouseDirection.z = 0;
            mouseDirection.Normalize();
            AttemptToDash();
        }

        if (shrineTrigger == true)
        {
            canMove = false;
            overrideDash = true;
        }


        if (Input.GetButtonDown("Dash") && shrineTrigger && hasClicked)
        {
            hasClicked = false;
            startShrineTimer = true;
            canMove = false;
            triggerAttack = true;
            disableInput = true;
            shrineTimer = 5;
            Debug.Log(shrine);
        }

        if(Input.GetButtonDown("Dash") && triggerAttack)
        {
            FastPool fastPool = FastPoolManager.GetPool(ring);
            fastPool.FastInstantiate(transform);

            clicks++;
            bossEnergyClicks++;
            slider.value = bossEnergyClicks;

            if(clicks == 10)
            {
                FMODUnity.RuntimeManager.PlayOneShot(Updrade1);
                _flashImage.Flash();
            }

            if (clicks == 20)
            {
                FMODUnity.RuntimeManager.PlayOneShot(Updrade2);
                _flashImage.Flash();
            }

            if (clicks == 30)
            {
                FMODUnity.RuntimeManager.PlayOneShot(Updrade3);
                _flashImage.Flash();
            }

            ActivateAttack();
        }
    }


    private void ActivateAttack()
    {
        orbClones = Instantiate(orbs, transform.position, Quaternion.identity);
        Destroy(orbClones, 5f);
    }

    private void AttemptToDash()
    {
        directionInput.x = PlayerInput.GetDirectionalInput().x;
        directionInput.y = PlayerInput.GetDirectionalInput().y;

        canDash = false;
        isDashing = true;
        canMove = false;
        dashTimeLeft = dashTime;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Shrine" && BossBehaviour.shrineActiveLeft || collision.tag == "Shrine" && BossBehaviour.shrineActiveRight)
        {
            shrineTrigger = true;
            shrine = collision.transform.position;
        }
    }


    public void FixedUpdate()
    {
        if (startShrineTimer == true)
        {
            shrineTimer -= Time.fixedDeltaTime;
            float newPosLerp = Mathf.Lerp(transform.position.y, shrine.y + 1.5f, 0.03f);
            transform.position = new Vector2(shrine.x, newPosLerp);
            moveDown = true;
        }

        if(shrineTimer < 0 && moveDown)
        {
            Destroy(orbClones);
            float newPosLerp = Mathf.Lerp(transform.position.y, shrine.y, 0.4f);
            float distance = Vector3.Distance(gameObject.transform.position, shrine);
            transform.position = new Vector2(shrine.x, newPosLerp);
            Debug.Log(distance);
            if (distance < 1)
            {
                moveDown = false;
                startShrineTimer = false;
                shrineTrigger = false;
                canMove = true;
                disableInput = false;
                hasClicked = true;
                triggerAttack = false;
                isDashing = false;
                canDash = true;
                overrideDash = false;
                clicks = 0;
            }
        }

        if (Mathf.Abs(PlayerInput.GetDirectionalInput().x) > 0)
        {
            acceleration = m_maxHorizontalSpeed / m_accelerationTimeFromRest * Time.fixedDeltaTime;
            currentSpeed += acceleration;
            currentSpeed = Mathf.Clamp(currentSpeed, 0, m_maxHorizontalSpeed);
            movementVelocityX = currentSpeed * directionInput.x;
        }
        else
        {
            deceleration = m_maxHorizontalSpeed / m_decelerationTimeToRest * Time.fixedDeltaTime;
            currentSpeed -= deceleration;
            currentSpeed = Mathf.Clamp(currentSpeed, 0, m_maxHorizontalSpeed);
            movementVelocityX = currentSpeed * directionInput.x;
        }

        if (Mathf.Abs(PlayerInput.GetDirectionalInput().y) > 0)
        {
            acceleration = m_maxHorizontalSpeed / m_accelerationTimeFromRest * Time.fixedDeltaTime;
            currentSpeed += acceleration;
            currentSpeed = Mathf.Clamp(currentSpeed, 0, m_maxHorizontalSpeed);
            movementVelocityY = currentSpeed * directionInput.y;
        }
        else
        {
            deceleration = m_maxHorizontalSpeed / m_decelerationTimeToRest * Time.fixedDeltaTime;
            currentSpeed -= deceleration;
            currentSpeed = Mathf.Clamp(currentSpeed, 0, m_maxHorizontalSpeed);
            movementVelocityY = currentSpeed * directionInput.y;
        }

        if (isDashing)
        {
            if (dashTimeLeft > 0)
            {
                dashSpeedX = mouseDirection.x * dashSpeed;
                dashSpeedY = mouseDirection.y * dashSpeed;
                GetComponent<Rigidbody2D>().velocity = new Vector2(dashSpeedX, dashSpeedY);
                movementVelocityX = dashSpeedX;
                movementVelocityY = dashSpeedY;
                dashTimeLeft -= Time.fixedDeltaTime;
            }
            if (dashTimeLeft <= 0)
            {
                endOfDash = true;
                endOfDashtimer = dashDeaccelerationTime;
                isDashing = false;
                
            }
        }
        else if (endOfDash)
        {
            endOfDashtimer -= Time.fixedDeltaTime;
            if (endOfDashtimer >= 0)
            {
                dashDeacellerationX = dashSpeedX / dashDeaccelerationTime * Time.fixedDeltaTime;
                dashDeacellerationY = dashSpeedY / dashDeaccelerationTime * Time.fixedDeltaTime;
                dashSpeedX -= dashDeacellerationX;
                dashSpeedY -= dashDeacellerationY;
                movementVelocityX = Mathf.Abs(dashSpeedX) * mouseDirection.x;
                movementVelocityY = Mathf.Abs(dashSpeedY) * mouseDirection.y;
            }
            else
            {
                movementVelocityY = 0;
                movementVelocityX = 0;
                canDash = true;
                endOfDash = false;
                canMove = true;
            }
        }

        if ((!isDashing || endOfDash || canMove) && !disableInput)
            GetComponent<Rigidbody2D>().velocity = new Vector2(movementVelocityX, movementVelocityY);

    }
}