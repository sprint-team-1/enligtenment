﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HealthSystem : MonoBehaviour
{
  
    // max health player can have
    public float maxPlayerHealth = 3;
    
    // current health for the player
    private float currentPlayerHealth;
    
    // value for the damage done to the player
    public float damageToPlayer = 1;
    
    // value for the heal if we need to
    public float healValue = 1;

    // put the health bar prefab in the scene and reference it to this variable
    public HealthBarUI healthBar;
    void Start()
    {
        // when the scene start, set the player health to the max value
        currentPlayerHealth = maxPlayerHealth;
        healthBar.SetMaxHealth(maxPlayerHealth);
    }


    private void LimitMaxHealth()
    {
        if (maxPlayerHealth > 3)
        {
            maxPlayerHealth = 3;
        }
    }

    private void GameOverCheck()
    {
        if (currentPlayerHealth <= 0)
        {
            SceneManager.LoadScene(0);
        }
    }

    // a function to call if the damage is dealt to the player
    void PlayerReceiveDamage()
    {
        // reduce the player's health
        currentPlayerHealth = currentPlayerHealth - damageToPlayer;
        // show the effect on the health bar
        healthBar.SetHealth(currentPlayerHealth);
        
        GameOverCheck();
    }



    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Hazard")
        {
            PlayerReceiveDamage();
            Destroy(collision.gameObject);
        }

        if (collision.gameObject.tag == "SweepHazard")
        {
            PlayerReceiveDamage();
            return;
        }
    }
}
