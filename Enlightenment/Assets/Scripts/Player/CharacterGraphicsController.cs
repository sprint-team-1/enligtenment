﻿using UnityEngine;

public class CharacterGraphicsController : MonoBehaviour
{
    private PlayerController m_player;
    private SpriteRenderer m_spriteRenderer;
    private Animator m_animator;
    public GameObject dustEffect;
    public GameObject dustPosition;

    [FMODUnity.EventRef]
    public string inputSound;

    [SerializeField] private float walkSpeed;

    private int IsWalkingLeftRightProperty;
    private int IsWalkingUpProperty;
    private int IsWalkingDownProperty;
    private int IsRollingUpProperty;
    private int IsRollingDownProperty;
    private int IsRollingLeftRightProperty;
    private int IsHitProperty;

    // Start is called before the first frame update
    private void Start()
    {
        m_player = GetComponent<PlayerController>();
        m_spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        m_animator = GetComponentInChildren<Animator>();

        IsWalkingLeftRightProperty = Animator.StringToHash("isWalkingLeftRight");
        IsWalkingUpProperty = Animator.StringToHash("isWalkingUp");
        IsWalkingDownProperty = Animator.StringToHash("isWalkingDown");
        IsRollingDownProperty = Animator.StringToHash("isRollingDown");
        IsRollingUpProperty = Animator.StringToHash("isRollingUp");
        IsRollingLeftRightProperty = Animator.StringToHash("isRollingLeftRight");

        //Not implemented yet
        IsHitProperty = Animator.StringToHash("isHit");

        InvokeRepeating("FootstepsSound", 0, walkSpeed);
    }

    // Update is called once per frame
    private void Update()
    {
        m_animator.SetBool(IsWalkingLeftRightProperty, m_player.IsWalkingLeftRight());
        m_animator.SetBool(IsWalkingUpProperty, m_player.IsWalkingUp());
        m_animator.SetBool(IsWalkingDownProperty, m_player.IsWalkingDown());
        m_animator.SetBool(IsRollingUpProperty, m_player.RollUp());
        m_animator.SetBool(IsRollingDownProperty, m_player.RollDown());
        m_animator.SetBool(IsRollingLeftRightProperty, m_player.RollLeftRight());

        switch (m_player.GetFacingDirection())
        {
            case PlayerController.FacingDirection.Left:
                m_spriteRenderer.flipX = true;
                break;

            case PlayerController.FacingDirection.Right:
            default:
                m_spriteRenderer.flipX = false;
                break;
        }

        if ((m_player.IsWalkingLeftRight() || m_player.IsWalkingUp() || m_player.IsWalkingDown()) && PlayerController.canDash)
        {
            FastPool fastPool = FastPoolManager.GetPool(dustEffect);
            fastPool.FastInstantiate(dustPosition.transform);
        }
    }

    private void FootstepsSound()
    {
        if ((m_player.IsWalkingLeftRight() || m_player.IsWalkingUp() || m_player.IsWalkingDown()) && PlayerController.canDash )
        {
            FMODUnity.RuntimeManager.PlayOneShot(inputSound);
        }
    }
}