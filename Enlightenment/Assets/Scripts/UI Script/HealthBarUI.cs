﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarUI : MonoBehaviour
{
    public Slider slider;

    public Gradient gradient;
    public Image fillValue;

    private void Start()
    {
        slider.maxValue = 3;
    }

    public void SetMaxHealth(float health)
    {
        // set the max health value at the start of the game, according to the max value
        slider.maxValue = health;
        slider.value = health;

        // when player has max health, health bar is green
        fillValue.color = gradient.Evaluate(1f);
    }
    
    public void SetHealth(float heath)
    {
        // setup the health value
        slider.value = heath;

       fillValue.color = gradient.Evaluate(slider.normalizedValue);
    }

}
