﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UITestingController : MonoBehaviour
{
    // max health player can have
    public float maxPlayerHealth = 100;
    
    // current health for the player
    private float currentPlayerHealth;
    
    // value for the damage done to the player
    public float damageToPlayer = 20;
    
    // value for the heal if we need to
    public float healValue = 20;

    // put the health bar prefab in the scene and reference it to this variable
    public HealthBarUI healthBar;
    void Start()
    {
        // when the scene start, set the player health to the max value
        currentPlayerHealth = maxPlayerHealth;
        healthBar.SetMaxHealth(maxPlayerHealth);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            PlayerReceiveDamage();
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            PlayerReceiveHealth();
        }
    }

    // a function to call if the damage is dealt to the player
    void PlayerReceiveDamage()
    {
        // reduce the player's health
        currentPlayerHealth = currentPlayerHealth - damageToPlayer;
        // show the effect on the health bar
        healthBar.SetHealth(currentPlayerHealth);
    }

    // heal function, if we want to implement healing
    void PlayerReceiveHealth()
    {
        currentPlayerHealth = currentPlayerHealth + healValue;
        
        healthBar.SetHealth(currentPlayerHealth);
    }
}
